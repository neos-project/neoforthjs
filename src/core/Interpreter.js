import {Parser} from './Parser'
import {voclistWords} from './buildins/voclistWords'
import {VocBuildins} from './vocs/VocBuildins'
import {VocNumbers} from './vocs/VocNumbers'
import {VocUser} from './vocs/VocUser'
import {VocList} from './VocList'
import {Stack} from './Stack'
import typeis from 'check-types'


export const StatusEnum = {
  ERR_VOC_SYSTEM: -5,
  ERR_DISPATCH: -4,
  ERR_WORD: -3,
  ERR_INIT_PARSER: -2,
  ERR_INIT: -1,
  OK: 0,
  RUNNING: 1,
  BYE: 2
}

const DEFAULT_VOCS = {
  VocBuildins: new VocBuildins(),
  VocNumbers: new VocNumbers(),
  VocUser: new VocUser()
}

const setDefaultVoclist = (state) => {
  state.voclist = new VocList([
    DEFAULT_VOCS.VocUser,
    DEFAULT_VOCS.VocNumbers,
    DEFAULT_VOCS.VocBuildins
  ])
}


export class Interpreter {
  constructor({io}, stackSize) {
    this.DEFAULT_VOCS = DEFAULT_VOCS
    this.io = io
    this.status = StatusEnum.ERR_INIT
    this.error = null
    this.base = 10
    this.compile = null
    this.newname = ''

    this.parsed = ''
    this.voc = null
    this.wid = null
    this.vm = {}

    this.setDefaultVoclist = setDefaultVoclist
    setDefaultVoclist(this)
    this.current = DEFAULT_VOCS.VocUser
    this.stack = new Stack(stackSize)

    if (!this.voclist || !this.stack) {
      this.stack = null
      this.voclist = null
      return
    }
    this.status = StatusEnum.OK
  }

  Interpret(source) {
    if (this.status === StatusEnum.ERR_INIT)
      return this.status

    this.parser = new Parser(source ?? '')
    if (!this.parser)
      return StatusEnum.ERR_INIT_PARSER

    this.status = StatusEnum.RUNNING
    while (this.status == StatusEnum.RUNNING) {
      const parsed = this.parser.NextWord()
      if (!parsed) {
        this.status = StatusEnum.OK
        break
      }

      const {voc, wid} = this.voclist.Find(this, parsed)
      if (!voc) {
        this.status = StatusEnum.ERR_WORD
        break
      }

      const result = voc.Dispatch(this, wid)
      if (typeis.number(result)) {
        this.status = result
      }
    }
    return this.status
  }
}
