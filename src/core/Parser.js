export class Parser {
  constructor(source_string) {
    this.source_buf = source_string
    this.offset = 0
  }
  Reset() { this.offset = 0 }

  EndOfBuffer() { return this.offset >= this.source_buf.length }

  CurrentChar() { return this.source_buf[this.offset] }
  PerviousChar() {
    return (this.offset > 0)? this.source_buf[--this.offset]: 0
  }
  NextChar() {
    return !this.EndOfBuffer()? this.source_buf[this.offset++]: 0
  }

  SkipDelims(isDelimFunc, param) {
    if (this.EndOfBuffer())
      return false
    while (
      !this.EndOfBuffer() &&
      isDelimFunc(this.CurrentChar(), param)
    )
      this.NextChar()
    return true
  }
  SkipNotDelims(isDelimFunc, param) {
    if (this.EndOfBuffer())
      return false
    while (
      !this.EndOfBuffer() &&
      !isDelimFunc(this.CurrentChar(), param)
    )
      this.NextChar()
    return true
  }

  is_space = (ch, param) => (' \t\n\r\0'.includes(ch))
  is_char = (ch, param) => (ch === param)

  ToChar(ch) {
    const start = this.offset
    if (!this.SkipNotDelims(this.is_char, ch))
      return null
    const end = this.offset
    const size = end - start
    this.NextChar()
    return this.source_buf.substr(start, size)
  }

  NextWord() {
    if (!this.SkipDelims(this.is_space))
      return null
    const start = this.offset
    if (!this.SkipNotDelims(this.is_space))
      return null
    const end = this.offset
    const size = end - start
    this.NextChar()
    return this.source_buf.substr(start, size)
  }
}
