export class Stack {
  MIN_SIZE = 32

  constructor(maxSize, inital) {
    this.data = inital ?? []
    this.maxSize = (maxSize < this.MIN_SIZE)? this.MIN_SIZE: maxSize
  }

  #doIfCountNot0(code) {
    return (this.Count() > 0)? code(this): null
  }

  MaxSize() { return this.maxSize }
  Count() { return this.data.length }
  Clear() { this.data = [] }

  Add(value) {
    if (this.Count() >= this.maxSize) {
      this.data.slice(this.maxSize)
      return false
    } else {
      this.data.unshift(value)
      return true
    }
  }
  Remove() {
    return this.#doIfCountNot0(() => (this.data.shift()))
  }
  First(n) {
    return this.data[0]
  }

  Push(value) {
    if (this.Count() >= this.maxSize) {
      this.data.slice(this.maxSize)
      return false
    } else {
      this.data.push(value)
      return true
    }
  }
  Pop(value) {
    return this.#doIfCountNot0(() => (this.data.pop()))
  }
  Top() {
    return this.#doIfCountNot0(() => (this.data[this.data.length - 1]))
  }

  Pick(n) {
    // todo
  }
  Roll(n) {
    // todo
  }

  ForEach(func, reverse) {
    const len = this.data.length
    for (let i = 0; i < len; i++) {
      const idx = reverse? len-1-i: i
      if (func(this.data[idx], i))
        break;
    }
  }
}