import {Stack} from './Stack'

export class VocList {
  constructor(inital) {
    this.context = new Stack(null, inital ?? [])
  }

  Find(state, name) {
    let result = {}
    this.context.ForEach(vocContext => {
      const wid = vocContext.Find(state, name)
      if (wid !== null && wid !== undefined) {
        result = {voc: vocContext, wid: wid}
        return true
      }
    }, true)
    return result
  }
}
