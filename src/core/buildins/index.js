// buildins
import {mathLogicWords} from './mathLogicWords'
import {stackWords} from './stackWords'
import {voclistWords} from './voclistWords'
import {vocWords} from './vocWords'
import {numWords} from './numWords'



const hello = {
  name: 'hello',
  data: (state, voc, wid) => {
    state.io.println('Hello Forth\nПривет Форт!')
  }
}

/*
const bye = {
  name: 'bye',
  data: (state, voc, wid) => {
    state.status = StatusEnum.BYE
  }
}
*/

const cr = {
  name: 'cr',
  data: (state, voc, wid) => { state.io.println() }
}

const backslash = {
  name: '\\',
  data: (state, voc, wid) => { state.parser.ToChar('\n') }
}

const bracket = {
  name: '(',
  data: (state, voc, wid) => { state.parser.ToChar(')') }
}

export const buildins = {
  hello, cr,
  '\\': backslash, '(': bracket,
  ...mathLogicWords,
  ...stackWords,
  ...voclistWords,
  ...vocWords,
  ...numWords
}
