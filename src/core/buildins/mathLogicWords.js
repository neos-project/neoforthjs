/////////////////////////////////////////////////
// Math words
const inc = {
  name: '1+',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Pop() + 1)
  }
}

const dec = {
  name: '1-',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Pop() - 1)
  }
}

const add = {
  name: '+',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Pop() + stack.Pop())
  }
}

const sub = {
  name: '-',
  data: (state, voc, wid) => {
    const stack = state.stack
    const b = stack.Pop()
    stack.Push(stack.Pop() - b)
  }
}

const mul = {
  name: '*',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Pop() * stack.Pop())
  }
}

const divmod = {
  name: '/%',
  data: (state, voc, wid) => {
    const stack = state.stack
    const b = stack.Pop()
    const a = stack.Pop()
    stack.Push(a % b)
    stack.Push(a / b)
  }
}


/////////////////////////////////////////////////
// Logic words
const _false = {
  name: 'false',
  data: (state, voc, wid) => { state.stack.Push(0) }
}

const _true = {
  name: 'true',
  data: (state, voc, wid) => { state.stack.Push(-1) }
}

const and = {
  name: 'and',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Pop() & stack.Pop())
  }
}

const or = {
  name: 'or',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Pop() | stack.Pop())
  }
}

const xor = {
  name: 'xor',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Pop() ^ stack.Pop())
  }
}

const not = {
  name: 'not',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(!stack.Pop())
  }
}

const neg = {
  name: 'neg',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(-stack.Pop())
  }
}


export const mathLogicWords = {
  inc, dec, add, sub, mul, divmod,
  false: _false, true: _true, and, or, xor, not, neg
}
