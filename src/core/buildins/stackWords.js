import {StackPrintStr} from '../../utils/stack'

/////////////////////////////////////////////////
// Stack words
const dot_s = {
  name: '.s',
  data: (state, voc, wid) => {
    state.io.print(`( ${StackPrintStr(state.stack,7)})`)
  }
}

const drop = {
  name: 'drop',
  data: (state, voc, wid) => { state.stack.Pop() }
}

const dup = {
  name: 'dup',
  data: (state, voc, wid) => {
    const stack = state.stack
    stack.Push(stack.Top())
  }
}

const qdup = {
  name: '?dup',
  data: (state, voc, wid) => {
    const stack = state.stack
    const x = stack.Top()
    if (x)
      stack.Push(x)
  }
}

const swap = {
  name: 'swap',
  data: (state, voc, wid) => {
    const stack = state.stack
    const b = stack.Pop()
    const a = stack.Pop()
    stack.Push(b)
    stack.Push(a)
  }
}

// pick
// roll


export const stackWords = {
  dot_s, drop, dup, qdup, swap
}
