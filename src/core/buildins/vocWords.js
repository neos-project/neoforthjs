import {StatusEnum} from '../Interpreter'
import {ModForthVM} from '../mods/ModForthVM'
import typeis from 'check-types'

/////////////////////////////////////////////////
// Voc words

const create = {
  name: 'create',
  data: (state, voc, wid) => {
    const current = state?.current
    if (!current) {
      return StatusEnum.ERR_VOC_SYSTEM
    }
    if (!state.compile) {
      const new_name = state.parser.NextWord()
      const wid = current.NewWord(state, new_name)
      if (!wid && wid !== 0) {
        return StatusEnum.ERR_VOC_SYSTEM
      }
    }
  }
}

const l_bracket = {
  name: '[',
  flags: {
    immediate: true
  },
  data: (state, voc, wid) => {
    state.compile = false
  }
}

const r_bracket = {
  name: ']',
  flags: {
    immediate: true
  },
  data: (state, voc, wid) => {
    state.compile = true
  }
}

const colon = {
  name: ':',
  flags: {
    immediate: true
  },
  data: (state, voc, wid) => {
    const result = create.data(state)
    if (!result) {
      const current = state?.current
      if (!current?.SetModule(state, ModForthVM)) {
        return StatusEnum.ERR_VOC_SYSTEM
      }
      r_bracket.data(state)
    }
  }
}

const semicolon = {
  name: ';',
  flags: {
    immediate: true
  },
  data: (state, voc, wid) => {
    if (state.compile) {
      const current = state?.current
      if (!current) {
        return StatusEnum.ERR_VOC_SYSTEM
      }
      l_bracket.data(state)
    }
  }
}

const _decompile = (state, word) => {
  state.io.print(`: ${word.name} `)
  if (typeis.nonEmptyArray(word.data)) {
    for (let it of word.data) {
      if (typeis.number(it)) {
        state.io.print(`${it} `)
      } else if (typeis.object(it) && !it?.flags?.hidden) {
        state.io.print(`${it.name} `)
      }
    }
  }
  state.io.print(`;`)
}

const see = {
  name: 'see',
  data: (state, voc_, wid_) => {
    const name = state.parser.NextWord()
    if (!name) {
      return
    }
    const {voc, wid} = state.voclist.Find(state, name)
    if (!voc) {
      state.status = StatusEnum.ERR_WORD
      return
    }
    const word = voc.GetWord(state, wid)
    if (word?.module === ModForthVM) {
      _decompile(state, word)
    }
  }
}

const recurse = {
  name: 'recurse',
  data: (state, voc, wid) => {
    const current = state?.current
    if (!current) {
      return StatusEnum.ERR_VOC_SYSTEM
    }
    if (!current.AddData(state, current.GetWord())) {
      return StatusEnum.ERR_VOC_SYSTEM
    }
  }
}

const immediate = {
  name: 'immdediate',
  data: (state, voc, wid) => {
    const current = state?.current
    if (!current) {
      return StatusEnum.ERR_VOC_SYSTEM
    }
    if (!current.SetFlag(state, 'immediate', true)) {
      return StatusEnum.ERR_VOC_SYSTEM
    }
  }
}

export const vocWords = {
  create, l_bracket, r_bracket, colon, semicolon, see,
  recurse, immediate
}
