/////////////////////////////////////////////////
// VocList words

const words = {
  name: 'words',
  data: (state, voc, wid1) => {
    let words_count = 0
    const context = state.voclist.context
    context.ForEach(voc => {
      let wid = voc.Next && voc.Next(state)
      if (wid !== undefined && wid !== null) {
        state.io.println(` == ${voc.name} ==`)
      }
      while (wid !== undefined && wid !== null) {
        const word = voc.GetWord(state, wid)
        const name = word?.name
        state.io.print(`${name} `)
        wid = voc.Next && voc.Next(state, wid)
        words_count += name.length + 1
        if (!wid || words_count >= 64) {
          state.io.println()
          words_count = 0
        }
      }
    }, true)
  }
}

const order = {
  name: 'order',
  data: (state, voc, wid1) => {
    const context = state.voclist.context
    state.io.print('Context: ')
    context.ForEach(voc => {
      state.io.print(`${voc.name} `)
    })
    state.io.println(`\nCurrent: ${state.current.name}`)
  }
}

const previous = {
  name: 'previous',
  data: (state, voc, wid) => {
    const context = state.voclist.context
    context.Remove()
  }
}

const also = {
  name: 'also',
  data: (state, voc, wid) => {
    const context = state.voclist.context
    context.Add(context.First())
  }
}

const only = {
  name: 'only',
  data: (state, voc, wid) => state.setDefaultVoclist(state)
}

const definitions = {
  name: 'definitions',
  data: (state, voc, wid) =>
    state.current = state.voclist.context.First()
}

export const voclistWords = {
  words, order, previous, also, only, definitions
}
