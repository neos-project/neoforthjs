import {StatusEnum} from '../Interpreter'
import typeis from 'check-types'



export const ModForthVM = {
  name: '%forthvm',
  data: (state, voc, wid) => {
    const word = voc.GetWord(state, wid)
    if (!word?.data || word?.module !== ModForthVM) {
      return StatusEnum.ERR_DISPATCH
    }
    const immediate = voc.GetFlag(state, "immediate", wid)
    if (state.compile && !immediate) {
      const current = state?.current
      if (!current) {
        return StatusEnum.ERR_VOC_SYSTEM
      }
      if (!current.AddData(state, word)) {
        return StatusEnum.ERR_VOC_SYSTEM
      }
    } else {
      const wordData = word.data
      if (wordData.length === 0)
        return
      const state_vm = { iptr: 0 }
      while (true) {
        const curWord = wordData[state_vm.iptr++]
        if (curWord) {
          state.vm = state_vm // todo: improve this
          if (curWord.module) {
            curWord.module.Execute(state, null, null, curWord)
          } else {
            curWord.data(state, null, null, wordData)
          }
        } else {
          return StatusEnum.ERR_DISPATCH
        }
        if (state_vm.iptr >= wordData.length)
          break
      }
    }
  }
}
