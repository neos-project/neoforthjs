import {StatusEnum} from '../Interpreter'
import typeis from 'check-types'



const lit = {
  flags: {
    hidden: true
  },
  data: (state, voc, wid, wordData) =>
    state.stack.Push(wordData[state.vm.iptr++])
}

export const ModNumbers = {
  name: '%numbers',
  data: (state, voc, wid) => {
    if (!typeis.number(wid)) {
      return StatusEnum.ERR_DISPATCH
    }
    const value = wid
    if (state.compile) {
      const current = state?.current
      if (!current) {
        return StatusEnum.ERR_VOC_SYSTEM
      }
      if (!current.AddData(state, lit)) {
        return StatusEnum.ERR_VOC_SYSTEM
      }
      current.AddData(state, value)
    } else {
      state.stack.Push(value)
    }
  }
}

