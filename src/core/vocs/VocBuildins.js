import {StatusEnum} from '../Interpreter'
import {buildins} from '../buildins'
import typeis from 'check-types'

export class VocBuildins {
  #buildins_keys = Object.keys(buildins)

  Find(state, name) {
    for (let wid of this.#buildins_keys) {
      const word = buildins[wid]
      if (word.name === name) {
        return wid
      }
    }
    return null
  }

  Next(state, wid) {
    const keys = this.#buildins_keys
    const isWid = typeis.string(wid)
    if (!wid && !isWid) {
      return keys[0]
    } else if (isWid) {
      let id = keys.findIndex(it => it === wid)
      return (++id < keys.length)? keys[id]: null
    } else {
      return null
    }
  }

  GetWord(state, wid) {
    if (wid) {
      return buildins[wid]
    } else {
      const keys = this.#buildins_keys
      return buildins[keys[keys.length - 1]]
    }
  }

  GetFlag(state, flag, wid) {
    const word = this.GetWord(state, wid)
    const flags = word?.flags
    return word && flags? flags[flag]: null
  }

  GetLength(state, wid) {
    return 0
  }

  // Unavailable
  GetModule(state, wid) { return null }
  NewWord(state, name) { return null }
  AddData(state, data, wid) { return false }
  SetFlag(state, flag, value, wid) { return false }
  SetModule(state, module, wid) { return false }

  name = '$buildins'
  module = (state, voc, wid) => {
    const context = state.voclist.context
    context.Remove()
    context.Add(voc)
  }
  Dispatch(state, wid, Module) {
    var result
    if (Module) {
      result = Module.data(state, this, wid)
    } else {
      const word_module = this.GetModule(state, wid)
      if (word_module) {
        result = word_module.data(state, this, wid)
      } else {
        const word = this.GetWord(state, wid)
        if (word) {
          result = word.data(state, this, wid)
        } else {
          result = StatusEnum.ERR_DISPATCH
        }
      }
    }
    return result
  }
}
