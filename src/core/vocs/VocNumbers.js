import {StatusEnum} from '../Interpreter'
import {buildins} from '../buildins'
import {ModNumbers} from '../mods/ModNumbers'

export class VocNumbers {
  Find(state, name) {
    let parsed = Number.parseInt(name, state.base)
    return (parsed == name)? parsed: null
  }

  GetModule(state, wid) {
    return ModNumbers
  }

  // Unavailable
  Next(state, wid) { return null }
  GetWord(state, wid) { return null }
  GetFlag(state, flag, wid) { return null }
  GetLength(state, wid) { return 0 }
  NewWord(state, name) { return null }
  AddData(state, data, wid) { return false }
  SetFlag(state, flag, value, wid) { return false }
  SetModule(state, module, wid) { return false }



  name = '$numbers'
  module = (state, voc, wid) => {
    const context = state.voclist.context
    context.Remove()
    context.Add(voc)
  }
  Dispatch(state, wid, Module) {
    var result
    if (Module) {
      result = Module.data(state, this, wid)
    } else {
      const word_module = this.GetModule(state, wid)
      if (word_module) {
        result = word_module.data(state, this, wid)
      } else {
        const word = this.GetWord(state, wid)
        if (word) {
          result = word.data(state, this, wid)
        } else {
          result = StatusEnum.ERR_DISPATCH
        }
      }
    }
    return result
  }
}
