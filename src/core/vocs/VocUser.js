import {StatusEnum} from '../Interpreter'
import {VocBuildins} from '../vocs/VocBuildins'
import {buildins} from '../buildins'
import {ModForthVM} from '../mods/ModForthVM'
import typeis from 'check-types'

export class VocUser {
  Find(state, name) {
    const words = this.words
    for (let wid in words) {
      const word = this.words[wid]
      if (word.name === name) {
        return wid
      }
    }
    return null
  }

  Next(state, wid) {
    const words = this.words
    const isWid = typeis.number(wid)
    if (!wid && !isWid) {
      return 0
    } else if (isWid) {
      return (++wid < words.length)? wid: null
    }
    return null
  }

  GetWord(state, wid) {
    return this.words[
      wid ?? this.words.length - 1
    ]
  }

  GetFlag(state, flag, wid) {
    const word = this.GetWord(state, wid)
    const flags = word?.flags
    return word && flags? flags[flag]: null
  }

  GetModule(state, wid) {
    const word = this.GetWord(state, wid)
    return word? word.module: null
  }

  GetLength(state, wid) {
    const word = this.GetWord(state, wid)
    return word? word.data?.length: null
  }

  NewWord(state, name) {
    if (typeis.nonEmptyString(name)) {
      const word = {
        name: name,
        module: ModForthVM,
        data: []
      }
      this.words.push(word)
      return this.words.length - 1
    }
    return null
  }

  AddData(state, data, wid) {
    const word = this.GetWord(state, wid)
    if (word && typeis.array(word?.data)) {
      word?.data?.push(data)
      return true
    } else {
      return false
    }
  }

  SetFlag(state, flag, value, wid) {
    const word = this.GetWord(state, wid)
    if (!word?.flags) word.flags = {}
    word.flags[flag] = value
  }

  SetModule(state, module, wid) {
    const word = this.GetWord(state, wid)
    if (word?.module) {
      word.module = module
      return true
    }
    return false
  }



  name = '$user'
  module = (state, voc, wid) => {
    const context = state.voclist.context
    context.Remove()
    context.Add(voc)
  }
  Dispatch(state, wid, Module) {
    var result
    if (Module) {
      result = Module.data(state, this, wid)
    } else {
      const word_module = this.GetModule(state, wid)
      if (word_module) {
        result = word_module.data(state, this, wid)
      } else {
        const word = this.GetWord(state, wid)
        if (word) {
          result = word.data(state, this, wid)
        } else {
          result = StatusEnum.ERR_DISPATCH
        }
      }
    }
    return result
  }
  words = [
    {
      name: 'test',
      module: ModForthVM,
      data: [
        buildins["hello"],
        buildins["words"]
      ]
    }
  ]
}
