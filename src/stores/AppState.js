import { writable } from 'svelte/store'

export const createAppstate = (initValue) => {
  const { subscribe, set, update } = writable(initValue ?? 0)

  const reset = () => set(initValue)
  const setScreenId = (id) => update(it => ({
    ...it, screenId: id ?? 0
  }))
  const setKeystate = (event) => update(it => ({
    ...it, keystate: {
      code: event.keyCode,
      char: event.charCode,
      ctrl: event.ctrlKey,
      alt: event.altKey,
      shift: event.shiftKey 
    }
  }))
  
  return { subscribe, reset, setScreenId, setKeystate }
}

export const Appstate = createAppstate({
  screenId: 0,
  keystate: {
    code: null,
    char: null,
    ctrl: false,
    alt: false,
    shift: false 
  }
});
