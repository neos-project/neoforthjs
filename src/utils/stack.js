export const StackPrintStr = (stack, min) => {
  let result = ''
  const count = stack.Count()
  if (min && count > min) {
    result += `[${count}.. ]`
  }
  if (!min || count < min) {
    min = count
  }
  for (let i = count - min; i < count; i++) {
    result += `${stack.data[i]} `
  }
  return result
}
