import MainView from './Main/MainView.svelte'

export const MAIN_VIEW = 0;

export const views = [
  {
    id: MAIN_VIEW,
    name: 'NeoForthJs',
    view: MainView
  }
]
